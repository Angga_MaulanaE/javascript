const Hallo = function(nama) {
    console.log("Hallo "+ nama);
}

Hallo("Angga");

const penjumlahan = function (a, b) {
    console.log("Hasil Penjumlahan = " + (a + b));
}

penjumlahan(4, 3);

const pengurangan = function (a, b){
    console.log("Hasil Perkurangan = "+ (a - b));
}

pengurangan(6, 2);

const perkalian = function (a, b) {
    console.log("Hasil Perkalian = " + (a * b));
}

perkalian(2, 3);

const pembagian = function (a, b) {
    console.log("Hasil Pembagian = " + (a / b));
}

pembagian(6, 2);

const rata = function (a, b) {
    console.log("Hasil Rata-rata = " + (a+b)/2);
}

rata(10, 6);